/*!
 * \file modbus/modbus_defs.h
 * \brief Definitions needed for modbus
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __MODBUS_DEFS_H__
#define __MODBUS_DEFS_H__

#ifdef MODBUS_EXPORTS
#define MODBUS_API __declspec(dllexport)
#elif MODBUS_USE
#define MODBUS_API __declspec(dllimport)
#else
#define MODBUS_API
#endif

#endif

