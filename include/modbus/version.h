/*!
 * \file modbus/version.h
 * \brief Version info for modbus
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define MODBUS_MAJOR_VERSION    3
#define MODBUS_MINOR_VERSION    1
#define MODBUS_RELEASE_NUMBER   6
#define MODBUS_VERSION_STRING   "modbus 3.1.6"

// Must be updated manually as well each time the version above changes
#define MODBUS_VERSION_NUM_DOT_STRING   "3.1.6"
#define MODBUS_VERSION_NUM_STRING       "030106"

// nothing should be updated below this line when updating the version

#define MODBUS_VERSION_NUMBER (MODBUS_MAJOR_VERSION * 1000) + (MODBUS_MINOR_VERSION * 100) + MODBUS_RELEASE_NUMBER
#define MODBUS_BETA_NUMBER      1
#define MODBUS_VERSION_FLOAT MODBUS_MAJOR_VERSION + (MODBUS_MINOR_VERSION/10.0) + (MODBUS_RELEASE_NUMBER/100.0) + (MODBUS_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define MODBUS_CHECK_VERSION(major,minor,release) \
    (MODBUS_MAJOR_VERSION > (major) || \
    (MODBUS_MAJOR_VERSION == (major) && MODBUS_MINOR_VERSION > (minor)) || \
    (MODBUS_MAJOR_VERSION == (major) && MODBUS_MINOR_VERSION == (minor) && MODBUS_RELEASE_NUMBER >= (release)))



