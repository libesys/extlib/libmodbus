/*!
 * \file modbus/version.h
 * \brief Version info for modbus_t
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __MODBUS_T_VERSION_H__
#define __MODBUS_T_VERSION_H__

// Bump-up with each new version
#define MODBUS_T_MAJOR_VERSION    0
#define MODBUS_T_MINOR_VERSION    0
#define MODBUS_T_RELEASE_NUMBER   1
#define MODBUS_T_VERSION_STRING   _T("modbus_t 0.0.1")

// Must be updated manually as well each time the version above changes
#define MODBUS_T_VERSION_NUM_DOT_STRING   "0.0.1"
#define MODBUS_T_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define MODBUS_T_VERSION_NUMBER (MODBUS_T_MAJOR_VERSION * 1000) + (MODBUS_T_MINOR_VERSION * 100) + MODBUS_T_RELEASE_NUMBER
#define MODBUS_T_BETA_NUMBER      1
#define MODBUS_T_VERSION_FLOAT MODBUS_T_MAJOR_VERSION + (MODBUS_T_MINOR_VERSION/10.0) + (MODBUS_T_RELEASE_NUMBER/100.0) + (MODBUS_T_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define MODBUS_T_CHECK_VERSION(major,minor,release) \
    (MODBUS_T_MAJOR_VERSION > (major) || \
    (MODBUS_T_MAJOR_VERSION == (major) && MODBUS_T_MINOR_VERSION > (minor)) || \
    (MODBUS_T_MAJOR_VERSION == (major) && MODBUS_T_MINOR_VERSION == (minor) && MODBUS_T_RELEASE_NUMBER >= (release)))

#endif

