/*!
 * \file modbus/version.h
 * \brief Version info for modbusexe
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __MODBUSEXE_VERSION_H__
#define __MODBUSEXE_VERSION_H__

// Bump-up with each new version
#define MODBUSEXE_MAJOR_VERSION    0
#define MODBUSEXE_MINOR_VERSION    0
#define MODBUSEXE_RELEASE_NUMBER   1
#define MODBUSEXE_VERSION_STRING   _T("modbusexe 0.0.1")

// Must be updated manually as well each time the version above changes
#define MODBUSEXE_VERSION_NUM_DOT_STRING   "0.0.1"
#define MODBUSEXE_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define MODBUSEXE_VERSION_NUMBER (MODBUSEXE_MAJOR_VERSION * 1000) + (MODBUSEXE_MINOR_VERSION * 100) + MODBUSEXE_RELEASE_NUMBER
#define MODBUSEXE_BETA_NUMBER      1
#define MODBUSEXE_VERSION_FLOAT MODBUSEXE_MAJOR_VERSION + (MODBUSEXE_MINOR_VERSION/10.0) + (MODBUSEXE_RELEASE_NUMBER/100.0) + (MODBUSEXE_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define MODBUSEXE_CHECK_VERSION(major,minor,release) \
    (MODBUSEXE_MAJOR_VERSION > (major) || \
    (MODBUSEXE_MAJOR_VERSION == (major) && MODBUSEXE_MINOR_VERSION > (minor)) || \
    (MODBUSEXE_MAJOR_VERSION == (major) && MODBUSEXE_MINOR_VERSION == (minor) && MODBUSEXE_RELEASE_NUMBER >= (release)))

#endif

