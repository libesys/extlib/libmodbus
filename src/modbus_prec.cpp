/*!
 * \file modbus/modbus_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// modbus.cpp : source file that includes just the standard includes
// modbus.pch will be the pre-compiled header
// modbus.obj will contain the pre-compiled type information

#include "modbus/modbus_prec.h"

// TODO: reference any additional headers you need in modbus_prec.h
// and not in this file

